# PQ calc

This is a simple script to convert between nit value and floating point data
using Dolby's Perceptual Quantizer (PQ) function as per the ST.2084 and ST.2100 reference papers.

Sources :

- [Wikipedia](https://en.wikipedia.org/wiki/Perceptual_quantizer)
- [BT.2100 reference paper](https://www.itu.int/rec/R-REC-BT.2100)

## Requirements

- python3
- [fire](https://pypi.org/project/fire/)

## Usage

```bash
python3 pq_calc.py VALUE [--inv] [--out=OUT] [-h] [--help]
```

Use the `--out` flag to select a specific format to output, among :
- `all` : will output in all the different formats in a list (default)
- `float` : will output a floating point value
- `8, 10, 12 or 16` : will output a 8, 10, 12 or 16 bit value

It has no effect if you use the `--inv` flag.

Use the `--inv` flag to convert from a float to a nit value using the EOTF. 
Otherwise will convert from a nit value to a floating point value.


