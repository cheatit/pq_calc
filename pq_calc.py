#!/usr/bin/env python3

import fire

# Constants

M1 = 1305 / 8192
M2 = 2523 / 32
C1 = 107 / 128
C2 = 2413 / 128
C3 = 2392 / 128


def nits_to_float(nits):
    """
    Converts a nit value into a floating point value using the inverse PQ curve (OETF)

    Parameters
    ----------
    nits: float or int, value in nits that you want to convert

    Returns
    -------
    float: the output of the conversion
    """
    assert nits >= 0
    assert nits <= 10000
    Y = nits / 10000
    return ((C1 + C2 * Y ** M1) / (1 + C3 * Y ** M1)) ** M2


def float_to_nits(x):
    """
    Converts a floating point value into a nit value using the PQ curve (EOTF)

    Parameters
    ----------
    x: float or int, value between 0 and 1 that you want to convert

    Returns
    -------
    float: the output of the conversion
    """
    assert x >= 0
    assert x <= 1
    return 10000 * (max([x ** (1 / M2) - C1, 0]) / (C2 - C3 * x ** (1 / M2))) ** (1 / M1)


def main(value, inv=None, out="all"):
    """
    Converts a nit value into a floating point value using the PQ inverse function, or
    converts a floating point value into a nit value if setting 'inv' to True

    Parameters
    ----------
    value: int or float, the value that we'd like to convert
    inv: bool, default False
        Set to True to do a float to nits conversion, otherwise will do
         a nits to float conversion by default
    out: str, default all, 
        Can be set to 'all', 'float', '8', '10', '12' or '16' to output the value
         in different formats
    Returns
    -------
    float: a floating point value corresponding to the input nit value, or a nit value corresponding to
        the input float valut if inv is set to True
    """
    if inv:
        return float_to_nits(value)
    else:
        if out in [8, 10, 12, 16]:
            return ((2 ** out) - 1) * nits_to_float(value)
        elif out in ["float", "fl", "f"]:
            return nits_to_float(value)
        elif out in ["all", "a"]:
            result = nits_to_float(value)
            print("float:", result)
            print("8 bit:", ((2 ** 8) - 1) * result)
            print("10bit:", ((2 ** 10) - 1) * result)
            print("12bit:", ((2 ** 12) - 1) * result)
            print("16bit:", ((2 ** 16) - 1) * result)


if __name__ == '__main__':
    fire.Fire(main)
